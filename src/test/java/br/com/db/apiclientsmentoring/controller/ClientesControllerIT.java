package br.com.db.apiclientsmentoring.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Objects;

import br.com.db.apiclientsmentoring.dto.OutClienteDTO;
import br.com.db.apiclientsmentoring.entities.PessoaFisica;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import br.com.db.apiclientsmentoring.dto.InClienteDTO;
import br.com.db.apiclientsmentoring.entities.Cliente;
import br.com.db.apiclientsmentoring.entities.PessoaJuridica;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestEntityManager
class ClientesControllerIT {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private TestEntityManager testEntityManager;

	private String toJson(Object object) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		return mapper.writeValueAsString(object);
	}

	@Test
	@Transactional
	void cadastrarPJCompletoTest() throws Exception {

		InClienteDTO novoCliente = InClienteDTO.builder().tipoPessoa("PJ").nome("Fabiano PJ").cpfCnpj("001234567890123")
				.setor("TI").build();

		MvcResult mvcResult = this.mockMvc
				.perform(post("/api/clientes/").contentType(MediaType.APPLICATION_JSON).content(toJson(novoCliente)))
				.andDo(print()).andExpect(status().isCreated()).andReturn();

		String idClienteCriado = Objects.requireNonNull(mvcResult.getResponse().getHeader("location")).split("/clientes/")[1];

		Cliente clienteCriado = testEntityManager.find(Cliente.class, Long.parseLong(idClienteCriado));

		PessoaJuridica pjCriado = (PessoaJuridica) clienteCriado;

		Assertions.assertEquals("Fabiano PJ", pjCriado.getNome());
		Assertions.assertEquals("001234567890123", pjCriado.getCpfCnpj());
		Assertions.assertEquals("TI", pjCriado.getSetor());
	}

	@Test
	@Transactional
	void cadastrarPJSemParametroObrigatorioTest() throws Exception {

		InClienteDTO novoCliente = InClienteDTO.builder().tipoPessoa("PJ").cpfCnpj("001234567890123").setor("TI")
				.build();

		this.mockMvc
				.perform(post("/api/clientes/").contentType(MediaType.APPLICATION_JSON).content(toJson(novoCliente)))
				.andDo(print()).andExpect(status().isBadRequest());
	}

	@Test
	@Transactional
	void cadastrarPFCompletoTest() throws Exception {

		InClienteDTO novoCliente = InClienteDTO.builder().tipoPessoa("PF").nome("Fabiano Dutra").cpfCnpj("01234567890")
				.sexo("M").estadoCivil("solteiro").build();

		MvcResult mvcResult = this.mockMvc
				.perform(post("/api/clientes/").contentType(MediaType.APPLICATION_JSON).content(toJson(novoCliente)))
				.andDo(print()).andExpect(status().isCreated()).andReturn();

		String idClienteCriado = Objects.requireNonNull(mvcResult.getResponse().getHeader("location")).split("/clientes/")[1];

		Cliente clienteCriado = testEntityManager.find(Cliente.class, Long.parseLong(idClienteCriado));

		PessoaFisica pfCriado = (PessoaFisica) clienteCriado;

		Assertions.assertEquals("Fabiano PJ", pfCriado.getNome());
		Assertions.assertEquals("01234567890", pfCriado.getCpfCnpj());
		Assertions.assertEquals("solteiro", pfCriado.getEstadoCivil());
		Assertions.assertEquals("M", pfCriado.getSexo());
	}

	@Test
	@Transactional
	void cadastrarPFSemParametroObrigatorioTest() throws Exception {

		InClienteDTO novoCliente = InClienteDTO.builder().tipoPessoa("PF").cpfCnpj("0123456789").sexo("M").build();

		this.mockMvc
				.perform(post("/api/clientes/").contentType(MediaType.APPLICATION_JSON).content(toJson(novoCliente)))
				.andDo(print()).andExpect(status().isBadRequest());
	}

	@Test
	@Transactional
	void consultarPorIdPJ() throws Exception {

		Cliente cliente = new PessoaJuridica("fabiano dutra", "545645645645646", "TI");

		testEntityManager.persistAndFlush(cliente);

		this.mockMvc.perform(get("/api/clientes/{idCliente}", cliente.getId())).andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.nome").value("fabiano dutra"))
				.andExpect(jsonPath("$.cpfCnpj").value("545645645645646"))
				.andExpect(jsonPath("$.setor").value("TI"))
				.andExpect(jsonPath("$.tipoPessoa").value("PJ"))
				.andExpect(jsonPath("$.sexo").isEmpty())
		;
	}

	@Test
	@Transactional
	void consultarPorIdPF() throws Exception {

		Cliente cliente = new PessoaFisica("fabiano dutra",  "masculino", "01234567890","solteiro");

		testEntityManager.persistAndFlush(cliente);

		var mvcResult = this.mockMvc.perform(get("/api/clientes/{idCliente}", cliente.getId())).andDo(print())
				.andExpect(status().isOk()).andReturn();

		ObjectMapper objectMapper = new ObjectMapper();
		var clienteAtual = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), OutClienteDTO.class);
		//todo pesquisar e implementar AssertJ

		OutClienteDTO clienteEsperado = new OutClienteDTO();
		clienteEsperado.setNome("fabiano dutra");
		clienteEsperado.setCpfCnpj("01234567890");
		clienteEsperado.setEstadoCivil("solteiro");
		clienteEsperado.setSexo("masculino");
		clienteEsperado.setTipoPessoa("PF");

		Assertions.assertEquals(clienteEsperado, clienteAtual);
	}

	@Test
	@Transactional
	void consultarVariosClientes() throws Exception{

	}

}
