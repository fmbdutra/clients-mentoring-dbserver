package br.com.db.apiclientsmentoring.entities;

import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//@Setter(value = AccessLevel.PRIVATE)
@Entity
@EqualsAndHashCode(of = "id")
public class Endereco {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@NotEmpty
	private String logradouro;
	@NotNull
	private int numero;

	private String complemento;
	@NotNull
	@NotEmpty
	private String cep;
	@NotNull
	@NotEmpty
	private String cidade;
	@NotNull
	@NotEmpty
	private String estado;

	private Long idCliente;
	
}
