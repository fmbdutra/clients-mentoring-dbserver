package br.com.db.apiclientsmentoring.entities;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper = true)
//@DiscriminatorColumn(name = "PJ")
public class PessoaJuridica extends Cliente {
	
	@NotNull
	@NotEmpty
	@Length(min = 14)
	private String cnpj;

	@NotNull
	@NotEmpty
	private String setor;
	
	public PessoaJuridica(String nome, String cnpj, String setor) {
		super(nome, null, "PJ", cnpj);
		this.cnpj = cnpj;
		this.setor = setor;
	}
	
	public PessoaJuridica() {
		
	}


}
