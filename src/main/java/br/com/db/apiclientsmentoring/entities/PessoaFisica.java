package br.com.db.apiclientsmentoring.entities;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper = true)
//@DiscriminatorColumn(name = "PF")
public class PessoaFisica extends Cliente {

	@NotNull
	@NotEmpty
	@Length(max = 9)
	private String sexo;

	@NotNull
	@NotEmpty
	private String estadoCivil;

	@NotNull
	@NotEmpty
	@Length(min = 11)
	private String cpf;

	public PessoaFisica(String nome, String sexo, String cpf, String estadoCivil) {
		super(nome, null, "PF", cpf);
		this.sexo = sexo;
		this.cpf = cpf;
		this.estadoCivil = estadoCivil;
	}
	
	public PessoaFisica() {
		
	}

}
