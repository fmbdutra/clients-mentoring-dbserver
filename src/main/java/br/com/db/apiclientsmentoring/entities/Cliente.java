package br.com.db.apiclientsmentoring.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter(value = AccessLevel.PRIVATE)
@EqualsAndHashCode(of = "id")
@Entity
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
//@DiscriminatorColumn(name = "TP_PES")
public abstract class Cliente {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull @NotEmpty @Length(min = 10)
	private String nome;
	
	@OneToOne
	@JoinColumn(name = "endereco_idCliente")
	Endereco endereco;
	
	@NotNull
	String cpfCnpj;
	
	public Cliente(String nome, Endereco endereco, String tipoCliente, String cpfCnpj) {
		this.nome = nome;
		this.endereco = endereco;
		this.cpfCnpj = cpfCnpj;
	}
}
