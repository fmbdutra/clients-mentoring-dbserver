package br.com.db.apiclientsmentoring.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InClienteDTO {
	
	@NotNull @NotEmpty @Length(min = 5)
	private String nome;
	@NotNull @NotEmpty
	private String tipoPessoa;
	@NotNull @NotEmpty @Length(min = 9)
	private String cpfCnpj;
	
	private String setor;
	private String estadoCivil;
	private String sexo;
}
