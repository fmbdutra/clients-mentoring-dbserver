package br.com.db.apiclientsmentoring.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class InEnderecoDTO {
	
	@NotNull
	@NotEmpty
	private String logradouro;
	@NotNull
	private int numero;

	private String complemento;
	@NotNull
	@NotEmpty
	private String cep;
	@NotNull
	@NotEmpty
	private String cidade;
	@NotNull
	@NotEmpty
	private String estado;
}
