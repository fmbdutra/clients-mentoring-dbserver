package br.com.db.apiclientsmentoring.dto;

import br.com.db.apiclientsmentoring.entities.Cliente;
import br.com.db.apiclientsmentoring.entities.PessoaFisica;
import br.com.db.apiclientsmentoring.entities.PessoaJuridica;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OutClienteDTO {

	private String nome;
	private String tipoPessoa;
	private String cpfCnpj;
	private String setor;
	private String sexo;
	private String estadoCivil;

	public OutClienteDTO(Cliente pessoa) {
		this.nome = pessoa.getNome();
		this.cpfCnpj = pessoa.getCpfCnpj();
		if(pessoa instanceof PessoaFisica){
			this.tipoPessoa = "PF";
			PessoaFisica pessoaFisica = (PessoaFisica) pessoa;
			this.sexo = pessoaFisica.getSexo();
			this.estadoCivil = pessoaFisica.getEstadoCivil();
		}
		if (pessoa instanceof PessoaJuridica) {
			this.tipoPessoa = "PJ";
			PessoaJuridica pessoaJuridica = (PessoaJuridica) pessoa;
			this.setor = pessoaJuridica.getSetor();
		}
	}

}
