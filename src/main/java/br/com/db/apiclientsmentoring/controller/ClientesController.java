package br.com.db.apiclientsmentoring.controller;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.db.apiclientsmentoring.dto.InClienteDTO;
import br.com.db.apiclientsmentoring.dto.InEnderecoDTO;
import br.com.db.apiclientsmentoring.dto.OutClienteDTO;
import br.com.db.apiclientsmentoring.entities.Cliente;
import br.com.db.apiclientsmentoring.entities.Endereco;
import br.com.db.apiclientsmentoring.service.ClienteService;


@RestController
@RequestMapping(value = "/api/clientes")
public class ClientesController {
	@Autowired
	private ClienteService clienteService;
	
//	@ApiOperation(value = "Cadastrar cliente")
	@PostMapping(path = { "", "/" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity cadastrarCliente(@Valid @RequestBody InClienteDTO clienteInput, UriComponentsBuilder uriBuilder) {

		Cliente novoCliente = clienteService.criarNovoCliente(clienteInput);

		URI uri = uriBuilder.path("/api/clientes/{idCliente}").buildAndExpand(novoCliente.getId()).toUri();

		return ResponseEntity.created(uri).build();
	}
	
//	@ApiOperation(value = "Busca por ID")
	@GetMapping(path ="/{idCliente}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OutClienteDTO> consultaUnicaPorId(@PathVariable Long idCliente) throws Exception {

		Optional<OutClienteDTO> response = clienteService.buscarClienteUnico(idCliente);

		return response.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}

	
//	@ApiOperation(value= "Lista todos os cliente")
	@GetMapping(path = { "/", "" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<OutClienteDTO> consultarCliente(
			@PageableDefault(sort = "id", direction = Direction.ASC, page = 0, size = 20) Pageable paginacao) {
		
		return clienteService.lista(paginacao);
	}
	
	@PostMapping(path = "/{idCliente}/endereco", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity cadastrarEndereco(@Valid @RequestBody InEnderecoDTO enderecoInput, @PathVariable Long idCliente, UriComponentsBuilder uriBuilder) throws Exception {

		Endereco novoEndereco = clienteService.criarNovoEnderecoParaCliente(enderecoInput, idCliente);

		URI uri = uriBuilder.path("/api/clientes/{idCliente}/endereco").buildAndExpand(novoEndereco.getIdCliente()).toUri();

		return ResponseEntity.created(uri).build();
	}
}
