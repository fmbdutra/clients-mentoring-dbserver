package br.com.db.apiclientsmentoring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication
@EnableSpringDataWebSupport
public class ApiClientsMentoringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiClientsMentoringApplication.class, args);
	}

}
