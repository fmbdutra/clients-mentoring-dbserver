package br.com.db.apiclientsmentoring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.db.apiclientsmentoring.entities.Endereco;

public interface EnderecoRepositoy extends JpaRepository<Endereco, Long>{

}