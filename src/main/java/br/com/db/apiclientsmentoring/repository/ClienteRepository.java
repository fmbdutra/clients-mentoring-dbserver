package br.com.db.apiclientsmentoring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.db.apiclientsmentoring.entities.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{

}
