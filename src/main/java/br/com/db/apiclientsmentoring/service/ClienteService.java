package br.com.db.apiclientsmentoring.service;

import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.db.apiclientsmentoring.dto.InClienteDTO;
import br.com.db.apiclientsmentoring.dto.InEnderecoDTO;
import br.com.db.apiclientsmentoring.dto.OutClienteDTO;
import br.com.db.apiclientsmentoring.entities.Cliente;
import br.com.db.apiclientsmentoring.entities.Endereco;
import br.com.db.apiclientsmentoring.entities.PessoaFisica;
import br.com.db.apiclientsmentoring.entities.PessoaJuridica;
import br.com.db.apiclientsmentoring.repository.ClienteRepository;
import br.com.db.apiclientsmentoring.repository.EnderecoRepositoy;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ClienteService {

	private final EnderecoRepositoy enderecoRepository;
	private final ClienteRepository clienteRepository;

	public ClienteService(EnderecoRepositoy enderecoRepository, ClienteRepository clienteRepository) {
		this.enderecoRepository = enderecoRepository;
		this.clienteRepository = clienteRepository;
	}

	public Cliente criarNovoCliente(final InClienteDTO cliente) {
		try {
			if ("PJ".equalsIgnoreCase(cliente.getTipoPessoa())) {
				PessoaJuridica pessoaJuridica = new PessoaJuridica(cliente.getNome(), cliente.getCpfCnpj(),
						cliente.getSetor());
				clienteRepository.save(pessoaJuridica);
				log.info("Cadastrada PJ: ID {}", pessoaJuridica.getId());

				return pessoaJuridica;
			} else {
				PessoaFisica pessoaFisica = new PessoaFisica(cliente.getNome(), cliente.getSexo(), cliente.getCpfCnpj(),
						cliente.getEstadoCivil());
				clienteRepository.save(pessoaFisica);

				log.info("Cadastrada PF: ID {}", pessoaFisica.getId());
				return pessoaFisica;
			}


		} catch (Exception e) {
			log.error("Erro ao cadastrar cliente: {}", e.getMessage());
			throw e;
		}

	}

	public Optional<OutClienteDTO> buscarClienteUnico(@NotNull Long id) throws Exception {
		return clienteRepository.findById(id).map(OutClienteDTO::new);
//			Optional<Cliente> cliente =
//		try {
//
//
//			if (cliente.isPresent()) {
//				dtoRetorno = OutClienteDTO(cliente);
//				dtoRetorno.setNome(cliente.get().getNome());
//			} else {
//				throw new NotFoundException();
//			}
//		} catch (Exception e) {
//			log.error(e.getMessage());
//			throw e;
//		}
//
//		return Optional.of(dtoRetorno);
	}

	public Page<OutClienteDTO> lista(Pageable paginacao) {
		
		Page<Cliente> clientes = clienteRepository.findAll(paginacao);
		
		return clientes.map(OutClienteDTO::new);
	}

	public Endereco criarNovoEnderecoParaCliente(@Valid InEnderecoDTO enderecoInput, Long idCliente) throws Exception {
		
		Endereco endereco = new Endereco();

		try {
			
			Optional<Cliente> cliente = clienteRepository.findById(idCliente);

			if(cliente.isPresent()) {
				endereco.setIdCliente(idCliente);
				
				endereco.setCep(enderecoInput.getCep());
				endereco.setCidade(enderecoInput.getCidade());
				if(!enderecoInput.getComplemento().isBlank()) endereco.setComplemento(endereco.getComplemento());
				endereco.setEstado(enderecoInput.getEstado());
				endereco.setLogradouro(enderecoInput.getLogradouro());
			} else {
				throw new NotFoundException();
			}			
					
			enderecoRepository.save(endereco);	
			return endereco;
		} catch (Exception e) {
			log.error("Erro ao cadastrar endereco no cliente " + idCliente +": {}", e.getMessage());
			throw e;
		}
	}
}
